package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.MissionAdapters.AttackAerialVehicle;
import Entities.Coordinates;

public class AttackMission extends Mission {
    private String target;

    public AttackMission(String target, Coordinates destination, String pilotName, AerialVehicle operatingVehicle)
            throws AerialVehicleNotCompatibleException {
        super(destination, pilotName, operatingVehicle);
        if (operatingVehicle instanceof AttackAerialVehicle) {
            this.target = target;
        } else {
            throw new AerialVehicleNotCompatibleException(operatingVehicle.getClass().getSimpleName() + " isn't meant for attack missions");
        }
    }

    @Override
    public String executeMission() {
        AttackAerialVehicle attacker = (AttackAerialVehicle) this.operatingAerialVehicle;

        return (this.pilotName + ": " + this.operatingAerialVehicle.getClass().getSimpleName() + " Attacking " + target +
                " with: " + attacker.getMissileType() + "X" + attacker.getAmountOfMissiles());
    }
}