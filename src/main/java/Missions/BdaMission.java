package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.MissionAdapters.BdaAerialVehicle;
import Entities.Coordinates;

public class BdaMission extends Mission {
    private String objective;

    public BdaMission(String objective, Coordinates destination, String pilotName, AerialVehicle operatingVehicle)
            throws AerialVehicleNotCompatibleException {
        super(destination, pilotName, operatingVehicle);
        if (operatingVehicle instanceof BdaAerialVehicle) {
            this.objective = objective;
        } else {
            throw new AerialVehicleNotCompatibleException(operatingVehicle.getClass().getSimpleName() + " isn't meant for BDA missions");
        }
    }

    @Override
    public String executeMission() {
        return (this.pilotName + ": " + this.operatingAerialVehicle.getClass().getSimpleName() + " taking pictures of " + objective +
                " with: " + ((BdaAerialVehicle) this.operatingAerialVehicle).getCameraType() + " camera");
    }
}