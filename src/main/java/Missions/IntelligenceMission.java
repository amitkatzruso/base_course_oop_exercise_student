package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.MissionAdapters.IntelligenceAerialVehicle;
import Entities.Coordinates;

public class IntelligenceMission extends Mission {
    private String region;

    public IntelligenceMission(String region, Coordinates destination, String pilotName, AerialVehicle operatingVehicle)
            throws AerialVehicleNotCompatibleException {
        super(destination, pilotName, operatingVehicle);
        if (operatingVehicle instanceof IntelligenceAerialVehicle) {
            this.region = region;
        } else {
            throw new AerialVehicleNotCompatibleException(operatingVehicle.getClass().getSimpleName() + " isn't meant for intelligence missions");
        }
    }

    @Override
    public String executeMission() {
        return (this.pilotName + ": " + this.operatingAerialVehicle.getClass().getSimpleName() + " Collecting Data in " + region +
                " with: sensor type: " + ((IntelligenceAerialVehicle) this.operatingAerialVehicle).getSensorType());
    }
}
