package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public abstract class Mission {
    protected Coordinates destinationCoords;
    protected String pilotName;
    protected AerialVehicle operatingAerialVehicle;

    public Mission(Coordinates destination, String pilotName, AerialVehicle operatingVehicle) {
        this.destinationCoords = destination;
        this.pilotName = pilotName;
        this.operatingAerialVehicle = operatingVehicle;
    }

    public void begin() {
        System.out.println("Beginning Mission!");
        this.operatingAerialVehicle.flyTo(destinationCoords);
    }

    public void cancel() {
        System.out.println("Abort Mission!");
        this.operatingAerialVehicle.land(this.operatingAerialVehicle.originBaseCoords());
    }

    public void finish() {
        System.out.println(this.executeMission());
        this.operatingAerialVehicle.land(this.operatingAerialVehicle.originBaseCoords());
        System.out.println("Finish Mission!");
    }

    public abstract String executeMission();
}