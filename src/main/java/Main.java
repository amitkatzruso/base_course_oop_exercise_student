import AerialVehicles.AerialVehicle;
import AerialVehicles.Drones.Shoval;
import AerialVehicles.Drones.Zik;
import AerialVehicles.F16;
import Equipments.CameraType;
import Equipments.MissileType;
import Equipments.SensorType;
import Entities.Coordinates;
import Missions.*;

public class Main {
    public static void main(String[] args) {
        Coordinates targetCoords = new Coordinates(20.153, 56.785);
        String targetDescription = "Terrorists Hideout";
        Coordinates homeBase = new Coordinates(0.0, 0.0);

        Mission firstMission = null;
        Mission secondMission = null;
        Mission thirdMission = null;

        AerialVehicle firstAircraft = new Zik(homeBase, SensorType.Elint, CameraType.Regular);
        AerialVehicle secondAircraft = new F16(homeBase, 4, MissileType.Python, CameraType.Thermal);
        AerialVehicle thirdAircraft = new Shoval(homeBase, 2, MissileType.Amram, SensorType.Elint, CameraType.NightVision);

        try {
            firstMission = new IntelligenceMission("Gaza", targetCoords, "Nissim Levi", secondAircraft);

            firstMission.begin();
            firstMission.finish();
        } catch (AerialVehicleNotCompatibleException exception) {
            System.out.println(exception.getMessage());
            System.out.println();
        }

        try {
            firstMission = new IntelligenceMission("Gaza", targetCoords, "Nissim Levi", firstAircraft);
            secondMission = new AttackMission(targetDescription, targetCoords, "Avi Cohen", secondAircraft);
            thirdMission = new BdaMission(targetDescription, targetCoords, "Idan Natan", thirdAircraft);

            firstMission.begin();
            firstMission.finish();

            System.out.println();

            secondMission.begin();
            thirdMission.begin();

            System.out.println();

            secondMission.finish();
            thirdMission.cancel();

            System.out.println();

            thirdMission.begin();
            thirdMission.finish();
        } catch (AerialVehicleNotCompatibleException exception) {
            System.out.println(exception.getMessage());
            System.out.println();
        }
    }
}