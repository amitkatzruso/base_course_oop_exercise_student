package AerialVehicles;


import AerialVehicles.MissionAdapters.AttackAerialVehicle;
import AerialVehicles.MissionAdapters.BdaAerialVehicle;
import Equipments.CameraType;
import Equipments.MissileType;
import Entities.Coordinates;

public class F16 extends CombatAircraft implements AttackAerialVehicle, BdaAerialVehicle {
    private int missilesAmount;
    private MissileType missileType;
    private CameraType cameraType;

    public F16(Coordinates originBaseCoords, int missilesAmount, MissileType missileType, CameraType cameraType) {
        super(originBaseCoords);
        this.missilesAmount = missilesAmount;
        this.missileType = missileType;
        this.cameraType = cameraType;
    }

    @Override
    public int getAmountOfMissiles() {
        return this.missilesAmount;
    }

    @Override
    public void setAmountOfMissiles(int newAmount) {
        this.missilesAmount = newAmount;
    }

    @Override
    public MissileType getMissileType() {
        return this.missileType;
    }

    @Override
    public void setMissileType(MissileType newMissileType) {
        this.missileType = newMissileType;
    }

    @Override
    public CameraType getCameraType() {
        return this.cameraType;
    }

    @Override
    public void setCameraType(CameraType newCameraType) {
        this.cameraType = newCameraType;
    }
}