package AerialVehicles;


import Entities.Coordinates;

public abstract class AerialVehicle {
    protected int hoursSinceLastRepair;
    protected FlightStatus currentStatus;
    protected Coordinates originBaseCoords;

    public AerialVehicle(Coordinates originBaseCoords) {
        this.currentStatus = FlightStatus.READY;
        this.hoursSinceLastRepair = 0;
        this.originBaseCoords = originBaseCoords;
    }

    public Coordinates originBaseCoords() {
        return this.originBaseCoords;
    }

    public void flyTo(Coordinates destination) {
        if ((this.currentStatus == FlightStatus.READY) || (this.currentStatus == FlightStatus.FLYING)) {
            System.out.println("Flying to: <" + destination + ">");
            this.currentStatus = FlightStatus.FLYING;
        } else {
            System.out.println("Aerial Vehicle isn't ready to fly");
        }
    }

    public void land(Coordinates destination) {
        System.out.println("Landing on: <" + destination + ">");
        this.check();
    }

    protected void check() {
        if (this.hoursSinceLastRepair >= this.maxFlightHours()) {
            this.currentStatus = FlightStatus.NOT_READY;
            this.repair();
        } else {
            this.currentStatus = FlightStatus.READY;
        }
    }

    protected void repair() {
        this.hoursSinceLastRepair = 0;
        this.currentStatus = FlightStatus.READY;
    }

    protected abstract int maxFlightHours();
}
