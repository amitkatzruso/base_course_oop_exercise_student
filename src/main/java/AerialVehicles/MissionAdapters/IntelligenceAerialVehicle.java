package AerialVehicles.MissionAdapters;

import Equipments.SensorType;
import jdk.nashorn.internal.objects.annotations.Getter;
import jdk.nashorn.internal.objects.annotations.Setter;

public interface IntelligenceAerialVehicle {
    @Getter
    public SensorType getSensorType();
    @Setter
    public void setSensorType(SensorType newSensorType);
}