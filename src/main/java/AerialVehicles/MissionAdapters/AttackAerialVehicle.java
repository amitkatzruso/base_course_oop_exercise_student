package AerialVehicles.MissionAdapters;

import Equipments.MissileType;
import jdk.nashorn.internal.objects.annotations.Getter;
import jdk.nashorn.internal.objects.annotations.Setter;

public interface AttackAerialVehicle {
    @Getter
    public int getAmountOfMissiles();
    @Setter
    public void setAmountOfMissiles(int newAmount);
    @Getter
    public MissileType getMissileType();
    @Setter
    public void setMissileType(MissileType newMissileType);
}