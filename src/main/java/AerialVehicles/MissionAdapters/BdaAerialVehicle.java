package AerialVehicles.MissionAdapters;

import Equipments.CameraType;
import jdk.nashorn.internal.objects.annotations.Getter;
import jdk.nashorn.internal.objects.annotations.Setter;

public interface BdaAerialVehicle {
    @Getter
    public CameraType getCameraType();
    @Setter
    public void setCameraType(CameraType newCameraType);
}