package AerialVehicles;

public enum FlightStatus {
    FLYING,
    READY,
    NOT_READY
}