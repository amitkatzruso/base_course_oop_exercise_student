package AerialVehicles.Drones;

import Entities.Coordinates;

public abstract class Haron extends Drone {
    private static final int MAX_FLIGHT_HOURS = 150;

    public Haron(Coordinates originBaseCoords) {
        super(originBaseCoords);
    }

    @Override
    public int maxFlightHours() {
        return MAX_FLIGHT_HOURS;
    }
}
