package AerialVehicles.Drones;

import AerialVehicles.MissionAdapters.AttackAerialVehicle;
import AerialVehicles.MissionAdapters.IntelligenceAerialVehicle;
import Equipments.MissileType;
import Equipments.SensorType;
import Entities.Coordinates;

public class Eitan extends Haron implements AttackAerialVehicle, IntelligenceAerialVehicle {
    private int missilesAmount;
    private MissileType missileType;
    private SensorType sensorType;

    public Eitan(Coordinates originBaseCoords, int missilesAmount, MissileType missileType, SensorType sensorType) {
        super(originBaseCoords);
        this.missilesAmount = missilesAmount;
        this.missileType = missileType;
        this.sensorType = sensorType;
    }

    @Override
    public int getAmountOfMissiles() {
        return this.missilesAmount;
    }

    @Override
    public void setAmountOfMissiles(int newAmount) {
        this.missilesAmount = newAmount;
    }

    @Override
    public MissileType getMissileType() {
        return this.missileType;
    }

    @Override
    public void setMissileType(MissileType newMissileType) {
        this.missileType = newMissileType;
    }

    @Override
    public SensorType getSensorType() {
        return this.sensorType;
    }

    @Override
    public void setSensorType(SensorType newSensorType) {
        this.sensorType = newSensorType;
    }
}
