package AerialVehicles.Drones;


import AerialVehicles.MissionAdapters.AttackAerialVehicle;
import AerialVehicles.MissionAdapters.BdaAerialVehicle;
import AerialVehicles.MissionAdapters.IntelligenceAerialVehicle;
import Equipments.CameraType;
import Equipments.MissileType;
import Equipments.SensorType;
import Entities.Coordinates;

public class Shoval extends Haron implements AttackAerialVehicle, IntelligenceAerialVehicle, BdaAerialVehicle {
    private int missilesAmount;
    private MissileType missileType;
    private SensorType sensorType;
    private CameraType cameraType;

    public Shoval(Coordinates originBaseCoords, int missilesAmount, MissileType missileType, SensorType sensorType, CameraType cameraType) {
        super(originBaseCoords);
        this.missilesAmount = missilesAmount;
        this.missileType = missileType;
        this.sensorType = sensorType;
        this.cameraType = cameraType;
    }

    @Override
    public int getAmountOfMissiles() {
        return this.missilesAmount;
    }

    @Override
    public void setAmountOfMissiles(int newAmount) {
        this.missilesAmount = newAmount;
    }

    @Override
    public MissileType getMissileType() {
        return this.missileType;
    }

    @Override
    public void setMissileType(MissileType newMissileType) {
        this.missileType = newMissileType;
    }

    @Override
    public SensorType getSensorType() {
        return this.sensorType;
    }

    @Override
    public void setSensorType(SensorType newSensorType) {
        this.sensorType = newSensorType;
    }

    @Override
    public CameraType getCameraType() {
        return this.cameraType;
    }

    @Override
    public void setCameraType(CameraType newCameraType) {
        this.cameraType = newCameraType;
    }
}

