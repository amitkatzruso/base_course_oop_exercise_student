package AerialVehicles.Drones;

import AerialVehicles.AerialVehicle;
import AerialVehicles.FlightStatus;
import Entities.Coordinates;

public abstract class Drone extends AerialVehicle {
    public Drone(Coordinates originBaseCoords) {
        super(originBaseCoords);
    }

    public String hoverOverLocation(Coordinates destination) {
        this.currentStatus = FlightStatus.FLYING;

        String message = "Hovering Over: " + destination;
        System.out.println(message);

        return message;
    }
}