package AerialVehicles.Drones;


import AerialVehicles.MissionAdapters.BdaAerialVehicle;
import AerialVehicles.MissionAdapters.IntelligenceAerialVehicle;
import Equipments.CameraType;
import Equipments.SensorType;
import Entities.Coordinates;

public class Zik extends Hermes implements IntelligenceAerialVehicle, BdaAerialVehicle {
    private SensorType sensorType;
    private CameraType cameraType;

    public Zik(Coordinates originBaseCoords, SensorType sensorType, CameraType cameraType) {
        super(originBaseCoords);
        this.sensorType = sensorType;
        this.cameraType = cameraType;
    }

    @Override
    public SensorType getSensorType() {
        return this.sensorType;
    }

    @Override
    public void setSensorType(SensorType newSensorType) {
        this.sensorType = newSensorType;
    }

    @Override
    public CameraType getCameraType() {
        return this.cameraType;
    }

    @Override
    public void setCameraType(CameraType newCameraType) {
        this.cameraType = newCameraType;
    }
}