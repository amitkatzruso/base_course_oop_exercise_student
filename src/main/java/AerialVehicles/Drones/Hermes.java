package AerialVehicles.Drones;

import Entities.Coordinates;

public abstract class Hermes extends Drone {
    private static final int MAX_FLIGHT_HOURS = 100;

    public Hermes(Coordinates originBaseCoords) {
        super(originBaseCoords);
    }

    @Override
    public int maxFlightHours() {
        return MAX_FLIGHT_HOURS;
    }
}