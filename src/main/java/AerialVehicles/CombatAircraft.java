package AerialVehicles;

import Entities.Coordinates;

public abstract class CombatAircraft extends AerialVehicle {
    private static final int MAX_FLIGHT_HOURS = 250;

    public CombatAircraft(Coordinates originBaseCoords) {
        super(originBaseCoords);
    }

    @Override
    public int maxFlightHours() {
        return MAX_FLIGHT_HOURS;
    }
}