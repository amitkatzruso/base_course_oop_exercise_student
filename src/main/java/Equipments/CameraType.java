package Equipments;

public enum CameraType {
    Regular,
    Thermal,
    NightVision
}